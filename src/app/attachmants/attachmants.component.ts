import { Component, OnInit } from '@angular/core';
import {AttachmantService} from '../serivces/attachmant-service';
import {Attachmant} from '../models/attachmant';

@Component({
  selector: 'app-attachmants',
  templateUrl: './attachmants.component.html',
  styleUrls: ['./attachmants.component.css']
})
export class AttachmantsComponent implements OnInit {
 private attachmants: Array<Attachmant>;
  constructor(private attachmantService: AttachmantService) { }

  ngOnInit() {
    this.attachmantService.getAttachmants().subscribe(attachmants => this.attachmants = attachmants);
  }

}
