import {Component, OnInit} from '@angular/core';
import {ProductService} from '../serivces/product-service';
import IProduct from '../interfaces/IProduct';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  private products: Array<IProduct>;

  constructor(private productService: ProductService) {
  }

  ngOnInit() {
    this.productService.getProducts().subscribe(products => this.products = products);
  }

}
