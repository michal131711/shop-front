import { Component, OnInit } from '@angular/core';
import {PriceList} from '../models/price-list';
import {PriceListService} from '../serivces/price-list-service';

@Component({
  selector: 'app-price-lists',
  templateUrl: './price-lists.component.html',
  styleUrls: ['./price-lists.component.css']
})
export class PriceListsComponent implements OnInit {
  private priceLists: Array<PriceList>;

  constructor(private priceListServiceService: PriceListService) { }

  ngOnInit() {
    this.priceListServiceService.getPriceLists().subscribe(priceLists => this.priceLists = priceLists);
  }

}
