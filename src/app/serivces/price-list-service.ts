import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {PriceList} from '../models/price-list';
import IPriceList from '../interfaces/IPriceList';

@Injectable({
  providedIn: 'root'
})
export class PriceListService {
  apiUrl: string = environment.apiURL;

  constructor(private http: HttpClient) { }

  getPriceLists(): Observable<Array<PriceList>> {
    return this.http
      .get(`${this.apiUrl}price/lists/`)
      .pipe(
        map(
          (priceListsRes: Array<IPriceList>) =>
            priceListsRes.map( priceListsReq => new PriceList(priceListsReq))
        )
      );
  }
}
