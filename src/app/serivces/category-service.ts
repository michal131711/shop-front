import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Category} from '../models/category';
import ICategory from '../interfaces/ICategory';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  apiUrl: string = environment.apiURL;

  constructor(private http: HttpClient) { }

  getCategories(): Observable<Array<Category>> {
    return this.http
      .get(`${this.apiUrl}category/`)
      .pipe(
        map(
          (iCategoriesRes: Array<ICategory>) =>
            iCategoriesRes.map( iCategoriesReq => new Category(iCategoriesReq))
        )
      );
  }
}
