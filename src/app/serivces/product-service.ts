import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Product} from '../models/product';
import IProduct from '../interfaces/IProduct';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  apiUrl: string = environment.apiURL;

  constructor(private http: HttpClient) { }

  getProducts(): Observable<Array<IProduct>> {
    return this.http
      .get(`${this.apiUrl}products/`)
      .pipe(
        map(
          (productsRes: Array<IProduct>) =>
            productsRes.map( productsReq => new Product(productsReq))
        )
      );
  }
}
