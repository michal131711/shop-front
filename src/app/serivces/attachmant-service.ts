import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import IAttachmant from '../interfaces/IAttachmant';
import {Attachmant} from '../models/attachmant';

@Injectable({
  providedIn: 'root'
})
export class AttachmantService {
  apiUrl: string = environment.apiURL;

  constructor(private http: HttpClient) { }

  getAttachmants(): Observable<Array<Attachmant>> {
    return this.http
      .get(`${this.apiUrl}attachment/`)
      .pipe(
        map(
          (iAttachmants: Array<IAttachmant>) =>
            iAttachmants.map( iAttachmantsReq => new Attachmant(iAttachmantsReq))
        )
      );
  }
}
