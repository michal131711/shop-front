import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import IUser from '../interfaces/IUser';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  apiUrl: string = environment.apiURL;

  constructor(private http: HttpClient) {
  }

  getUsers(): Observable<Array<User>> {
    return this.http
      .get(`${this.apiUrl}user/`)
      .pipe(
        map(
          (usersRes: Array<IUser>) =>
            usersRes.map( userRes => new User(userRes))
        )
      );
  }
}
