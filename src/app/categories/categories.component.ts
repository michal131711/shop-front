import {Component, OnInit} from '@angular/core';
import {Category} from '../models/category';
import {CategoryService} from '../serivces/category-service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  private categories: Array<ICategory>;

  constructor(private categoryServiceService: CategoryService) {
  }

  ngOnInit() {
    this.categoryServiceService.getCategories().subscribe(categories => this.categories = categories);
  }

}
