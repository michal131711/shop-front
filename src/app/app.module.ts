import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import { UserComponent } from './user/user.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { PriceListsComponent } from './price-lists/price-lists.component';
import { AttachmantsComponent } from './attachmants/attachmants.component';
import { CategoriesComponent } from './categories/categories.component';
import { ProductsComponent } from './products/products.component';

const appRoutes: Routes = [
  {path: 'user', component: UserComponent},
  {path: 'price/list', component: PriceListsComponent},
  {path: 'attachmant', component: AttachmantsComponent},
  {path: 'categories', component: CategoriesComponent},
  {path: 'products', component: ProductsComponent},
  {path: '', redirectTo: 'user', pathMatch: 'full'},
  {path: '**', redirectTo: 'user', pathMatch: 'full'},
];

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    PriceListsComponent,
    AttachmantsComponent,
    CategoriesComponent,
    ProductsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  entryComponents: [],

  bootstrap: [AppComponent]
})
export class AppModule { }
