import {Component, OnInit} from '@angular/core';
import {User} from '../models/user';
import {UserService} from '../serivces/user-service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  private users: Array<User>;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.userService.getUsers().subscribe(users => this.users = users);
  }

}
