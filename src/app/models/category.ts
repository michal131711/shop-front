import ICategory from '../interfaces/ICategory';
import {Product} from './product';

export class Category implements ICategory {
  id: number;
  name: string;
  products: Array<Product>;

  constructor(category: ICategory) {
    Object.assign(this, category);
  }
}
