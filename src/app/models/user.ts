import IUser from '../interfaces/IUser';
import IPriceList from '../interfaces/IPriceList';

export class User implements IUser {
  active: boolean;
  email: string;
  id: number;
  name: string;
  priceLists: Array<IPriceList>;
  roles: Array<any>;
  surname: string;

  constructor(user: IUser) {
    Object.assign(this, user);
  }
}
