import IProduct from '../interfaces/IProduct';
import ICategory from '../interfaces/ICategory';
import {Category} from './category';

export class Product implements IProduct {
  id: number;
  name: string;
  description: string;
  category: Array<ICategory>;

  constructor(product: Product) {
    Object.assign(this, product);
  }
}
