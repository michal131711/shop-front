import IPriceList from '../interfaces/IPriceList';
import IUser from '../interfaces/IUser';

export class PriceList implements IPriceList{
  description: string;
  id: number;
  name: string;

  constructor(iPriceList: IPriceList) {
    Object.assign(this, iPriceList);
  }
}
