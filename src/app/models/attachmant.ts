import IAttachmant from '../interfaces/IAttachmant';

export class Attachmant implements IAttachmant {
  extension: string;
  id: number;
  name: string;

  constructor(attachmant: IAttachmant) {
    Object.assign(this, attachmant);
  }
}
