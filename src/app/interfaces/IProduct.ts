import ICategory from './ICategory';

export default interface IProduct {
  id: number;
  name: string;
  description: string;
  category: Array<ICategory>;
}
