export default interface IAttachmant {
  id: number;
  name: string;
  extension: string;
}
