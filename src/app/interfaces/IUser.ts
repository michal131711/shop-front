import IPriceList from './IPriceList';

export default interface IUser {
  id: number;
  name: string;
  surname: string;
  email: string;
  active: boolean;
  roles: Array<any>;
  priceLists: Array<IPriceList>;
}
