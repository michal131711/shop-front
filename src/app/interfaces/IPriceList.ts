export default interface IPriceList {
  id: number;
  name: string;
  description: string;
}
